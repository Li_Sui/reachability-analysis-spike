### HOW TO RUN

Main class: `nz.ac.massey.testreachability.Main`

paramters: args[0]=CCTs dir, args[1]=doop dir (You have to unzip the dataset)

programs that currently being tested: "commons-collections-3.2.1","fitjava-1.1","javacc-5.0","jFin_DateMath-R1.0.1","oscache-2.4.1". 

output format(in console): `targeMethod:Number of tests.` For example, `java.lang.Object#hashCode:1759` means There are 1759 tests can reach java.lang.Object#hashCode

output format(in csv): each column `program ,testclass, test method , reachable method class, reachable method name, reachable in CCT, reachable in context-insens. analysis, reachable in context-sens. analysis, reachable in reflection analysis`