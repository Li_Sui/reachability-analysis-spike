package nz.ac.massey.testreachability;

import com.google.common.collect.Multimap;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.*;

/**
 * @author Li
 */
public class Tests {

    @Test
    public void testBuildDoopMethod() {
        String method1="<Driver_builtinTest22: void main(java.lang.String[])>/java.lang.Throwable.getMessage/10";
        String method2="<thread-group-init>/0"; // not a method, should be ignored
        String method3="<java.time.LocalDateTime: java.time.LocalDateTime of(java.time.LocalDate,java.time.LocalTime)>/java.time.LocalDateTime.<init>/0";
        String method4="<org.apache.commons.collections.map.ListOrderedMap$ListOrderedMapIterator: java.lang.Object setValue(java.lang.Object)>/java.util.Map.put/0";

        MethodSpec expect1=new MethodSpec("Driver_builtinTest22","main","([Ljava/lang/String;)V");
        MethodSpec expect3=new MethodSpec("java.time.LocalDateTime","of","(Ljava/time/LocalDate;Ljava/time/LocalTime;)Ljava/time/LocalDateTime;");
        MethodSpec expect4=new MethodSpec("org.apache.commons.collections.map.ListOrderedMap$ListOrderedMapIterator","setValue","(Ljava/lang/Object;)Ljava/lang/Object;");

        Assert.assertEquals(expect1,DoopUtil.buildDoopMethod(method1));
        Assert.assertNull(DoopUtil.buildDoopMethod(method2));
        Assert.assertEquals(expect3,DoopUtil.buildDoopMethod(method3));
        Assert.assertEquals(expect4,DoopUtil.buildDoopMethod(method4));
    }

    /**
     * expect java.util.HashMap#keySet -> Test#test01#()V
     */
    @Test
    public void testProcessCCTs(){
        Multimap<String,MethodSpec> map= Main.processCCTs("src/test/resources/CCTs");
        Assert.assertEquals(1, map.keySet().size());
        Assert.assertEquals("java.util.HashMap#keySet",map.keySet().iterator().next());
        MethodSpec expectTest=new MethodSpec("Test","test01","()V");
        Assert.assertTrue(map.get("java.util.HashMap#keySet").contains(expectTest));
    }

    /**
     * expect java.util.HashMap#keySet -> Test#test01#()V
     */
    @Test
    public void testProcessSCG() throws Exception{
        Multimap<String,MethodSpec> map= Main.processSCG("src/test/resources/doop");
        Assert.assertEquals(1, map.keySet().size());
        Assert.assertEquals("java.util.HashMap#keySet",map.keySet().iterator().next());
        MethodSpec expectTest=new MethodSpec("Test","test01","()V");
        Assert.assertTrue(map.get("java.util.HashMap#keySet").contains(expectTest));
    }
}