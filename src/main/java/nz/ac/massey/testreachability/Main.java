package nz.ac.massey.testreachability;
import com.google.common.base.Preconditions;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.traverse.BreadthFirstIterator;

import java.io.*;
import java.nio.charset.Charset;
import java.util.*;


/**
 *
 * suggest to run with large memory -Xmx15g
 * @author Jens, Li
 */
public class Main {
    //for debugging, we only do 5 programs
    public static List<String> programList= Arrays.asList("commons-collections-3.2.1","fitjava-1.1","javacc-5.0","jFin_DateMath-R1.0.1","oscache-2.4.1");
    private static Logger LOGGER = Logger.getLogger("analyse-CCTs");
    static List<String> targetMethodsList;

    static{
        try {
            String path=Main.class.getClassLoader().getResource("targetMethods.txt").getFile();
            targetMethodsList=FileUtils.readLines(new File(path), Charset.defaultCharset());
        }catch (IOException e){
            e.printStackTrace();
        }
    }
    public static void main (String[] args) throws Exception {
        BasicConfigurator.configure();
        if(args.length!=2){
            System.err.println("args[0]=CCTs dir, args[1]=doop dir");
            return;
        }
        File cctsDir=new File(args[0]);
        File doopDir= new File(args[1]);
        //write to a huge csv file
        FileWriter fw = new FileWriter("output.csv");
        BufferedWriter bw = new BufferedWriter(fw);
        bw.write("program\treachableMethod\ttestMethod\treachable in CCTs\treachable in SCGs\n");//TODO: will add more columns
        //iterate each program
        for(String program: programList) {
            Multimap<String, MethodSpec> cctMap = processCCTs(cctsDir+"/"+program);
            Multimap<String, MethodSpec> scgMap = processSCG(doopDir+"/lib-setup/"+program);// we use the results from lib-setup for now
//            Summary.print(program,cctMap,true);
//            Summary.print(program,scgMap,false);
            //iterate CCTs
            for(String key: cctMap.keySet()) {
                for(MethodSpec value: cctMap.get(key)) {
                    bw.write(program + "\t"+key+"\t"+value.getClassName()+"#"+value.getMethodName()+"\t1\t0\n");
                }
            }
            //iterate SCGs
            for(String key: scgMap.keySet()) {
                for(MethodSpec value: scgMap.get(key)) {
                    bw.write(program + "\t"+key+"\t"+value.getClassName()+"#"+value.getMethodName()+"\t0\t1\n");
                }
            }
        }
        bw.close();
        fw.close();
    }

    /**
     *
     * @param programDir
     * @return Multimap<String,MethodSpec>= <targetMethod,List<testMethod>>
     * @throws Exception
     */
    public static Multimap<String,MethodSpec> processCCTs(String programDir){
        LOGGER.info(programDir+", processing CCT....");
        Multimap<String,MethodSpec> map= HashMultimap.create();
        Iterator<File> fileIterator = FileUtils.iterateFiles(new File(programDir), null, true);
        while(fileIterator .hasNext()){
            File file=fileIterator.next();
            if(entryIsCCT(file.getName())) {
                MethodSpec sourceMethod=null;
                try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        String[] tokens = line.split("\t");
                        String[] tokens2 = tokens[0].split(",");
                        //extract info
                        int depth = Integer.valueOf(tokens[2]);
                        String classLoaderName = tokens2[0];
                        String className = tokens2[1];
                        String methodName = tokens2[2];
                        String descriptor = tokens2[3];
                        String threadName = tokens[1];

                        if (isTargetMethod(depth,threadName,className,methodName)&&sourceMethod!=null) {
                            map.put(className+"#"+methodName,sourceMethod);
                        }
                        if (isTestMethod(classLoaderName, threadName, depth, methodName)) {
                            sourceMethod=new MethodSpec(className,methodName,descriptor );
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return map;
    }
    /**
     *
     * @param programDir
     * @return Multimap<String,MethodSpec>= <targetMethod,List<testMethod>>
     * @throws Exception
     */
    public static Multimap<String,MethodSpec> processSCG(String programDir) throws Exception{
        LOGGER.info(programDir+", processing SCG....");
        Multimap<String,MethodSpec> map= HashMultimap.create();
        File doopBaseFile=new File(programDir+"/base/AnyCallGraphEdge.csv");
        Graph<MethodSpec, DefaultEdge> graph = new DefaultDirectedGraph<>(DefaultEdge.class);
        Set<MethodSpec> sourceMethodsSet=new HashSet<>();
        //construct a directed graph
        for (String line: FileUtils.readFileToString(doopBaseFile, Charset.defaultCharset()).split(System.getProperty("line.separator"))) {
            MethodSpec caller = DoopUtil.buildDoopMethod(line.split("\t")[0]);
            MethodSpec callee = DoopUtil.buildDoopMethod(line.split("\t")[1]);
            if(caller!=null && callee!=null ) {
                graph.addVertex(caller);
                graph.addVertex(callee);
                graph.addEdge(caller, callee);
                if (caller.getClassName().matches("Driver\\_.*Test.*") && !callee.getMethodName().equals("<init>") && !callee.getMethodName().equals("<clinit>")) {
                    sourceMethodsSet.add(callee);
                }
            }
        }
        //perform breadthFirstSearch to find if the target method is reachable from the source(test)
        for(MethodSpec test:sourceMethodsSet){
            BreadthFirstIterator<MethodSpec, DefaultEdge> breadthFirstIterator = new  BreadthFirstIterator<MethodSpec, DefaultEdge>(graph,test);
            while (breadthFirstIterator .hasNext()) {
                MethodSpec method =breadthFirstIterator.next();
                if(targetMethodsList.contains(method.getClassName()+"#"+method.getMethodName())){
                    map.put(method.getClassName()+"#"+method.getMethodName(),test);
                }
            }
        }
        return map;
    }

    /**
     *  a method is a test method must satisfy: loaded by AppClassLoader, in a main thread, depth is 2, methodName is not <init> nor <clinit>
     */
    private static boolean isTestMethod (String classLoaderName, String threadName, int depth, String methodName) {
        return classLoaderName.startsWith("sun.misc.Launcher$AppClassLoader")&& threadName.equals("main")&&depth==2 &&
                !methodName.equals("<init>") && !methodName.equals("<clinit>");
    }

    /**
     * a method is a target method must satisfy: in a main thread, depth >2, contained in targetMethodsList and a additionally, sourceInvocation has been parsed
     */
    private static boolean isTargetMethod (int depth, String threadName,String className,String methodName) {
        return  depth > 2 && threadName.equals("main") && targetMethodsList.contains(className+"#"+methodName) ;
    }

    private static boolean entryIsCCT(String name) {
        return name.contains("Driver_") && name.endsWith(".csv");
    }

    private static void writeToFile(StringBuilder sb, String outputDir) throws Exception{
        IOUtils.write(sb.toString(),new FileOutputStream(new File(outputDir)), Charset.defaultCharset());
    }

}
