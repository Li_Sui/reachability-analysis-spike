package nz.ac.massey.testreachability;

import org.apache.commons.lang3.StringUtils;

/**
 * @author Li
 * build doop methods
 */
public class DoopUtil {

    /**
     * convert Doop methods
     * @param method
     * @return converted string
     */
    public static MethodSpec buildDoopMethod(String method){
        if(method.contains("-")){// invalid identifier, usually for identifying thread, which is not considered in this context
            return null;
        }
        String className= StringUtils.substringBetween(method,"<",":");
        String returnType= parseString(StringUtils.substringBetween(method,": ","(").split("\\s")[0]);
        String methodName= StringUtils.substringBetween(method,": ","(").split("\\s")[1];
        String doopParameters=StringUtils.substringBetween(method,"(",")");
        String parameters="";
        if(doopParameters.contains(",")) {
            for (String p : doopParameters.split(",")){
                parameters=parameters+parseString(p);
            }
        }else{
            parameters=parseString(doopParameters);
        }
        if(doopParameters.equals("")){
            parameters="";
        }
        String descriptor="("+parameters+")"+returnType;
        return new MethodSpec(className,methodName,descriptor);
    }

    private static String parseString(String s){
        String result="";

        if(s.equals("void")) {
            return "V";
        }
        if(isPrimitive(s)) {
            if (s.startsWith("int")) {
                result = "I";
            }

            if (s.startsWith("byte")) {
                result = "B";
            }
            if (s.startsWith("long")) {
                result = "J";
            }

            if (s.startsWith("float")) {
                result = "F";
            }

            if (s.startsWith("double")) {
                result = "D";
            }
            if (s.startsWith("short")) {
                result = "S";
            }

            if (s.startsWith("char")) {
                result = "C";
            }

            if (s.startsWith("boolean")) {
                result = "Z";
            }

            if(s.contains("[]")){
                int count=StringUtils.countMatches(s,"[");
                String tmp="";
                for(int i=0;i<count;i++){
                    tmp=tmp+"[";
                }
                result=tmp+result;
            }
            return  result;
        }else {
            result="L"+s.replaceAll("\\.","/").replaceAll("\\[\\]","")+";";

            if (s.contains("[]")) {
                int count = StringUtils.countMatches(s, "[");
                String tmp = "";
                for (int i = 0; i < count; i++) {
                    tmp = tmp + "[";
                }
                result = tmp + result;
            }


            return result;
        }
    }

    private static boolean isPrimitive(String s) {

        if (s.startsWith("int") || s.startsWith("byte") || s.startsWith("long") || s.startsWith("float")
                || s.startsWith("double") || s.startsWith("short") || s.startsWith("char") || s.startsWith("boolean")) {
            return true;
        } else {
            return false;
        }
    }
}
