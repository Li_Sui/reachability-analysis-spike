package nz.ac.massey.testreachability;

import com.google.common.collect.Multimap;

public class Summary {


    public static void print(String program, Multimap<String,MethodSpec> map, boolean isCCT){
        if(isCCT){
            System.out.println("------CCTs Summary, program:"+program+"------");
            for(String targetMethod: map.keySet()){
                System.out.println(targetMethod+":"+map.get(targetMethod).size());
            }
        }else{
            System.out.println("------SCG Summary, program:"+program+"------");
            for(String targetMethod: map.keySet()){
                System.out.println(targetMethod+":"+map.get(targetMethod).size());
            }
        }

    }
}
